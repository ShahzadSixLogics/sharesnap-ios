//
//  MainViewController.swift
//  SnapShare
//
//  Created by Muhammad Shazhad on 12/01/2022.
//

import UIKit
import MultipeerConnectivity
import Network

class MainViewController: UIViewController {
    
    // MARK:- IBOutlets
    /// name should be in the format "viewTypeName" e.g. labelAge
    @IBOutlet var constraintCollection: [NSLayoutConstraint]!
    @IBOutlet var labelCollection: [UILabel]!
    @IBOutlet var buttonCollection: [UIButton]!
    @IBOutlet var textFieldCollection: [UITextField]!
    
    @IBOutlet weak var collectionViewImages: UICollectionView!
    @IBOutlet weak var imageViewShared: UIImageView!
    
    // MARK:- Internal Variables
    
    var didLoadViews = false
    var connection: NWConnection?
    
    let resources: [DownloadResource] = [
        DownloadResource(id: "1", source: URL(string: "https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Superman_S_symbol.svg/1200px-Superman_S_symbol.svg.png")!, destinationName: "superman.png"),
        DownloadResource(id: "2", source: URL(string: "https://cdn.wallpapersafari.com/42/86/xbfrMv.jpg")!, destinationName: "batman.jpg"),
        DownloadResource(id: "3", source: URL(string: "https://w0.peakpx.com/wallpaper/119/928/HD-wallpaper-spiderman-logo-spider-man-marvel-logo.jpg")!, destinationName: "spiderman.jpg")
    ]
    
    lazy var concurrent: Concurrent = {
        let configuration = ConcurrentConfiguration(mode: .parallel(max: 3))
        return Concurrent(configuration: configuration)
    }()
    
    // MARK:- Extternal Variables
    
    
    // MARK:- Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialiseVariables()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if !self.didLoadViews {
            self.didLoadViews = true
            configureViews()
        }
    }
    
    // MARK:- Custom Methods
    
    func initialiseVariables() {
        
        //configure multipeer session
        //        multiPeerSession.setupPeerWithDisplayName(displayName: UIDevice.current.name)
        //        multiPeerSession.setupSession()
        //        multiPeerSession.advertiseSelf(advertise: true)
        //        multiPeerSession.delegate = self
        
        let connection = NWConnection(host: NWEndpoint.Host("localhost"), port: NWEndpoint.Port(integerLiteral: 5555), using: .tcp)//NWConnection(host: serverEndpoint, port: portEndpoint, using: .tcp)
        connection.stateUpdateHandler = { [weak self] (newState) in
            switch newState {
            case .ready:
                debugPrint("TcpReader.ready to send")
                self?.receive()
            case .failed(let error):
                debugPrint("TcpReader.client failed with error \(error)")
            case .setup:
                debugPrint("TcpReader.setup")
            case .waiting(_):
                debugPrint("TcpReader.waiting")
            case .preparing:
                debugPrint("TcpReader.preparing")
            case .cancelled:
                debugPrint("TcpReader.cancelled")
            @unknown default:
                debugPrint("TcpReader.@unknown")
            }
        }
    }
    
    func receive() {
        guard let connection = self.connection else { return }
        connection.receive(minimumIncompleteLength: 1, maximumLength: 8192) { (content, context, isComplete, error) in
            debugPrint("\(Date()) TcpReader: got a message \(String(describing: content?.count)) bytes")
            if let content = content {
                //self.delegate.gotData(data: content, from: self.hostname, port: self.port)
            }
            if connection.state == .ready && isComplete == false {
                self.receive()
            }
        }
    }
    
    func configureViews() {
        /*
         self.view.layoutIfNeeded()
         for constraint in self.constraintCollection {
         constraint.resize()
         }
         
         for label in self.labelCollection {
         label.font = UIFont.setFont(withFontFamily: label.font.fontName, withSize: label.font.pointSize)
         }
         
         for button in self.buttonCollection {
         button.titleLabel?.font = UIFont.setFont(withFontFamily: button.titleLabel!.font.fontName, withSize: button.titleLabel!.font.pointSize)
         }
         
         for textField in self.textFieldCollection {
         textField.font = UIFont.setFont(withFontFamily: textField.font!.fontName, withSize: textField.font!.pointSize)
         }
         */
        
        self.collectionViewImages.isHidden = true
        self.imageViewShared.isHidden = true
        self.collectionViewImages.delegate = self
        self.collectionViewImages.dataSource = self
        self.collectionViewImages.register(cellClass: ImageCollectionViewCell.self)
    }
    
    func sendImageData(with data: Data) {
        do {
            try multiPeerSession.session.send(data, toPeers: multiPeerSession.session.connectedPeers, with: .reliable)
        } catch {
            print("Error sending: \(error.localizedDescription)")
        }
    }
    
    func showAlertWithOneAction(alertTitle: String, alertMessage: String, completion: @escaping() -> Void) {
        let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
            completion()
        }))
        self.present(alert, animated: true)
    }
    
    // MARK:- Navigation Methods
}

// MARK:- IBActions

extension MainViewController {
    // Actions name should be like "eventViewTypeAction" e.g. "backButtonAction"
    
    @IBAction func connectedButtonAction(_ sender: Any) {
        //        guard multiPeerSession.session != nil else { return }
        //        multiPeerSession.setupBrowser()
        //        multiPeerSession.browser.delegate = self
        //        self.present(multiPeerSession.browser, animated: true, completion: nil)
    }
    
    @IBAction func downloadButtonAction(_ sender: Any) {
        self.collectionViewImages.isHidden = false
        try? concurrent.cleanDownloadsDirectory()
        do {
            try concurrent.enqueueMultipleDownloads(forResources: resources)
        } catch let error {
            debugPrint(error.localizedDescription)
        }
        
        collectionViewImages.reloadData()
    }
}

// MARK:- Networking

extension MainViewController {
    
}


// MARK:- UITableView

extension MainViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return resources.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeue(cellClass: ImageCollectionViewCell.self, forIndexPath: indexPath)
        
        let resource = resources[indexPath.row]
        cell.bindWith(downloadResource: resource)
        if let task = concurrent.downloadTask(forResource: resource) {
            cell.bindWith(task: task)
        }
        
        concurrent.addDelegate(cell)
        cell.reuse = { [weak self] c in
            self?.concurrent.removeDelegate(c)
        }
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = view.frame.size.width/3
        return CGSize(width: width * 0.92, height: 150)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let resource = resources[indexPath.row]
        if let url = FileManager.concurrentPath(forResourceWithName: resource.destinationName) {
            
            guard navigationItem.title != "No Connection" else {
                showAlertWithOneAction(alertTitle: "No Connection", alertMessage: "You are not connected with any user.", completion: {
                })
                return
            }
            
            guard let image = UIImage(contentsOfFile: url.path) else { return }
            
            sendImageData(with: image.pngData()!)
        }else {
            showAlertWithOneAction(alertTitle: "Error", alertMessage: "Image is not downloaded yet! Please wait.", completion: {
            })
        }
    }
    
}


// MARK:- Multipeer Session Handling
extension MainViewController: MultiPeerSessionDelegate {
    
    func changedState(state: MCSessionState, of peer: MCPeerID) {
        guard state == .connected else {
            navigationItem.title = "No Connection"
            navigationItem.leftBarButtonItem?.isEnabled = true
            multiPeerSession.advertiseSelf(advertise: true)
            return
        }
        navigationItem.title = "Connected"
        navigationItem.leftBarButtonItem?.isEnabled = false
        multiPeerSession.advertiseSelf(advertise: false)
    }
    
    func receivedData(data: Data, from peer: MCPeerID) {
        if let image : UIImage = UIImage(data: data) {
            self.imageViewShared.isHidden = false
            self.imageViewShared.image = image
        }
    }
}

// MARK:- MCBrowserViewControllerDelegate
extension MainViewController: MCBrowserViewControllerDelegate {
    func browserViewControllerDidFinish(_ browserViewController: MCBrowserViewController) {
        multiPeerSession.browser.dismiss(animated: true, completion: nil)
    }
    
    func browserViewControllerWasCancelled(_ browserViewController: MCBrowserViewController) {
        multiPeerSession.browser.dismiss(animated: true, completion: nil)
    }
}


class NetworkService {
    
    lazy var connection: NWConnection = {
        // Create the connection
        
        let connection = NWConnection(host: NWEndpoint.Host("localhost"), port: NWEndpoint.Port(integerLiteral: 5555), using: .tcp)//NWConnection(host: "x.x.x.x", port: 1234, using: self.parames)
        connection.stateUpdateHandler = self.listenStateUpdate(to:)
        return connection
        
    }()
    
    lazy var parames: NWParameters = {
        let parames = NWParameters(tls: nil, tcp: self.tcpOptions)
        if let isOption = parames.defaultProtocolStack.internetProtocol as? NWProtocolIP.Options {
            isOption.version = .v4
        }
        parames.preferNoProxies = true
        parames.expiredDNSBehavior = .allow
        parames.multipathServiceType = .interactive
        parames.serviceClass = .background
        return parames
    }()
    
    lazy var tcpOptions: NWProtocolTCP.Options = {
        let options = NWProtocolTCP.Options()
        options.enableFastOpen = true // Enable TCP Fast Open (TFO)
        options.connectionTimeout = 5 // connection timed out
        return options
    }()
    
    let queue = DispatchQueue(label: "hostname", attributes: .concurrent)
    
    private func listenStateUpdate(to state: NWConnection.State) {
        // Set the state update handler
        switch state {
        case .setup:
            // init state
            debugPrint("The connection has been initialized but not started.")
        case .waiting(let error):
            debugPrint("The connection is waiting for a network path change with: \(error)")
            self.disconnect()
        case .preparing:
            debugPrint("The connection in the process of being established.")
        case .ready:
            // Handle connection established
            // this means that the handshake is finished
            debugPrint("The connection is established, and ready to send and receive data.")
            self.receiveData()
            self.sendHeartbeat()
        case .failed(let error):
            debugPrint("The connection has disconnected or encountered an: \(error)")
            self.disconnect()
        case .cancelled:
            debugPrint("The connection has been canceled.")
        default:
            break
        }
    }
    
    // MARK: - Socket I/O
    func connect() {
        // Start the connection
        self.connection.start(queue: self.queue)
    }
    
    func disconnect() {
        // Stop the connection
        self.connection.stateUpdateHandler = nil
        self.connection.cancel()
    }
    
    private func sendPacket() {
        var packet: Data? // do something for heartbeat packet
        self.connection.send(content: packet, completion: .contentProcessed({ (error) in
            if let err = error {
                // Handle error in sending
                debugPrint("encounter an error with: \(err) after send Packet")
            } else {
                // Send has been processed
            }
        }))
    }
    
    private func receiveData() {
        self.connection.receive(minimumIncompleteLength: 1, maximumLength: 8192) { [weak self] (data, context, isComplete, error) in
            guard let weakSelf = self else { return }
            if weakSelf.connection.state == .ready && isComplete == false, var data = data, !data.isEmpty {
                // do something for detect heart packet
                //weakSelf.parseHeartBeat(&data)
            }
        }
    }
    
    // MARK: - Heartbeat
    private func sendHeartbeat() {
        // sendHeartbeatPacket
        self.sendPacket()
        
    }
    
}
