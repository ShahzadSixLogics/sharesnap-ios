//
//  ImageCollectionViewCell.swift
//  SnapShare
//
//  Created by Muhammad Shahzad on 12/01/2022.
//  Copyright © 2022 Muhammad Shahzad. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    
    // MARK:- IBOutlets
    /// name should be in the format "viewTypeName" e.g. labelAge
    @IBOutlet var constraintCollection: [NSLayoutConstraint]!
    @IBOutlet var labelCollection: [UILabel]!
    @IBOutlet var buttonCollection: [UIButton]!
    @IBOutlet var textFieldCollection: [UITextField]!
    
    @IBOutlet weak var imageViewDownloaded: UIImageView!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    // MARK:- Internal Variables
    
    var resource: DownloadResource!
    var reuse: ((ImageCollectionViewCell) -> Void)?
    
    // MARK:- Extternal Variables
    
    
    // MARK:- Overriden Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureViews()
    }
    
    // MARK:- Custom Methods
    
    func configureViews() {
        /*
         self.layoutIfNeeded()
         for constraint in self.constraintCollection {
         constraint.resize()
         }
         
         for label in self.labelCollection {
         label.font = UIFont.setFont(withFontFamily: label.font.fontName, withSize: label.font.pointSize)
         }
         
         for button in self.buttonCollection {
         button.titleLabel?.font = UIFont.setFont(withFontFamily: button.titleLabel!.font.fontName, withSize: button.titleLabel!.font.pointSize)
         }
         
         for textField in self.textFieldCollection {
         textField.font = UIFont.setFont(withFontFamily: textField.font!.fontName, withSize: textField.font!.pointSize)
         }
         */
    }
    
    func bindWith(downloadResource resource: DownloadResource) {
        progressView.isHidden = true
        activityIndicator.isHidden = true
        activityIndicator.stopAnimating()
        
        self.resource = resource
        
        if let url = FileManager.concurrentPath(forResourceWithName: resource.destinationName) {
            guard let image = UIImage(contentsOfFile: url.path) else { return }
            imageViewDownloaded.image = image
        }
    }
    
    func bindWith(task: URLSessionDownloadTask) {
        
        switch task.state {
        case .canceling:
            progressView.isHidden = true
            activityIndicator.isHidden = true
        case .suspended:
            activityIndicator.isHidden = false
            progressView.isHidden = true
            activityIndicator.startAnimating()
        case .running:
            let progress = Float(task.countOfBytesReceived) / Float(task.countOfBytesExpectedToReceive)
            if progress >= 0.0 && progress <= 1.0 {
                activityIndicator.stopAnimating()
                activityIndicator.isHidden = true
                progressView.isHidden = false
                progressView.progress = progress
            }
        case .completed:
            activityIndicator.isHidden = true
            progressView.isHidden = true
            activityIndicator.stopAnimating()
            
            if task.countOfBytesExpectedToReceive == task.countOfBytesReceived {
                // downloaded sucessfully
            } else {
                // error occured
            }
        @unknown default:
            fatalError()
        }
    }
    
    fileprivate func bindWith(downloadedFile file: DownloadedFile) {
        progressView.isHidden = true
        activityIndicator.stopAnimating()
        activityIndicator.isHidden = true
        
        do {
            let url = try file.url()
            guard let image = UIImage(contentsOfFile: url.path) else { return }
            imageViewDownloaded.image = image
            //taskStateLabel.text = "Completed"
            progressView.isHidden = true
            activityIndicator.stopAnimating()
            activityIndicator.isHidden = true
        } catch let error {
            imageViewDownloaded.image = nil
            print(error.localizedDescription)
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        resource = nil
        imageViewDownloaded.image = nil
        reuse?(self)
    }
}

extension ImageCollectionViewCell: ConcurrentDelegate {
    func concurrent(_ concurrent: Concurrent, didStartDownloadingResource resource: DownloadResource, withTask task: URLSessionDownloadTask) {
        guard resource == self.resource else { return }
        bindWith(task: task)
    }
    
    func concurrent(_ concurrent: Concurrent, didUpdateStatusOfTask task: URLSessionDownloadTask, relatedToResource resource: DownloadResource) {
        guard resource == self.resource else { return }
        bindWith(task: task)
    }
    
    func concurrent(_ concurrent: Concurrent, didFinishDownloadingResource resource: DownloadResource, toFile file: DownloadedFile) {
        guard resource == self.resource else { return }
        bindWith(downloadedFile: file)
    }
}
