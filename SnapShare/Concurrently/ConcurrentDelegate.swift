//
//  ConcurrentDelegate.swift
//  SnapShare
//
//  Created by Muhammad Shahzad on 13/01/2022.
//  Copyright © 2022 Muhammad Shahzad. All rights reserved.
//

import Foundation

public protocol ConcurrentDelegate: AnyObject {
    /// Invoked when download did start for paricular resource (download task is added to the queue)
    ///
    /// - Parameters:
    ///   - concurrent: Concurrent object
    ///   - resource: Resource which download did start
    ///   - task: URLSessionDownloadTask for reading state and observing progress
    func concurrent(_ concurrent: Concurrent, didStartDownloadingResource resource: DownloadResource, withTask task: URLSessionDownloadTask)
    
    /// Invoked when next chunk of data is downloaded of particular item
    ///
    /// - Parameters:
    ///   - concurrent: Concurrent object
    ///   - task: URLSessionDownloadTask for reading progress and state
    ///   - resource: Resource related with download
    func concurrent(_ concurrent: Concurrent, didUpdateStatusOfTask task: URLSessionDownloadTask, relatedToResource resource: DownloadResource)
    
    /// Invoked when particular resource downloading is finished
    ///
    /// - Parameters:
    ///   - concurrent: Concurrent object
    ///   - resource: Resource related with download
    ///   - file: Object that contains relative path to file
    func concurrent(_ concurrent: Concurrent, didFinishDownloadingResource resource: DownloadResource, toFile file: DownloadedFile)
    
    /// Invoked when finished and maybe error occured during downloading particular resource
    ///
    /// - Parameters:
    ///   - concurrent: Concurrent object
    ///   - error: Error that occured during downloading
    ///   - task: URLSessionDownloadTask for getting status / progress
    ///   - resource: Downloaded resource
    func concurrent(_ concurrent: Concurrent, didCompleteWithError error: Error?, withTask task: URLSessionDownloadTask, whenDownloadingResource resource: DownloadResource)
    
    /// Invoked when queue finished since was empty
    ///
    /// - Parameters:
    ///   - concurrent: Concurrent object
    ///   - error: Optional error that occured during download, if nil job completed sucessfuly
    func concurrent(_ concurrent: Concurrent, didFinishWithMostRecentError error: Error?)
    
}

public extension ConcurrentDelegate {
    // Default implementations for making methods optional
    
    func concurrent(_ concurrent: Concurrent, didStartDownloadingResource resource: DownloadResource, withTask task: URLSessionDownloadTask) {
        
    }
    
    func concurrent(_ concurrent: Concurrent, didUpdateStatusOfTask task: URLSessionDownloadTask, relatedToResource resource: DownloadResource) {
        
    }
    
    func concurrent(_ concurrent: Concurrent, didCompleteWithError error: Error?, withTask task: URLSessionDownloadTask, whenDownloadingResource resource: DownloadResource) {
        
    }
    
    func concurrent(_ concurrent: Concurrent, didFinishWithMostRecentError error: Error?) {
        
    }
}
