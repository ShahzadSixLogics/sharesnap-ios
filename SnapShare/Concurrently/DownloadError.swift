//
//  DownloadError.swift
//  SnapShare
//
//  Created by Muhammad Shahzad on 13/01/2022.
//  Copyright © 2022 Muhammad Shahzad. All rights reserved.
//

import Foundation

public enum DownloadError: Error {
    case missingURL
}
