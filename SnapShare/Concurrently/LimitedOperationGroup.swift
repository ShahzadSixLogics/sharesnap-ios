//
//  LimitedOperationGroup.swift
//  SnapShare
//
//  Created by Muhammad Shahzad on 12/01/2022.
//  Copyright © 2022 Muhammad Shahzad. All rights reserved.
//

import Foundation
internal class LimitedOperationGroup {
    
    let queue: OperationQueue
    let limit: Int
    
    internal init(limit: Int = 1) {
        self.queue = OperationQueue()
        queue.maxConcurrentOperationCount = limit
        self.limit = limit
    }
    
    internal func addAsyncOperation(operation: Operation) {
        queue.addOperation(operation)
    }
}
