//
//  MultipeerConnectivity.swift
//  SnapShare
//
//  Created by Muhammad Shazhad on 12/01/2022.
//

import UIKit
import MultipeerConnectivity

protocol MultiPeerSessionDelegate {
    func changedState(state: MCSessionState, of peer: MCPeerID)
    func receivedData(data: Data, from peer: MCPeerID)
}

var multiPeerSession = MultiPeerSession.handler

class MultiPeerSession: NSObject {
    
    static var handler = MultiPeerSession()
    var peerID: MCPeerID!
    var session: MCSession!
    var browser: MCBrowserViewController!
    var advertiser: MCAdvertiserAssistant?
    var delegate: MultiPeerSessionDelegate?
    
    override init() {
        super.init()
        setupPeerWithDisplayName(displayName: UIDevice.current.name)
        setupSession()
        advertiseSelf(advertise: true)
    }
    
    func setupPeerWithDisplayName(displayName: String) {
        peerID = MCPeerID(displayName: displayName)
    }
    
    func setupSession() {
        session = MCSession(peer: peerID)
        session.delegate = self
    }
    
    func setupBrowser() {
        browser = MCBrowserViewController(serviceType: "snapshare", session: session)
    }
    
    func advertiseSelf(advertise: Bool) {
        guard advertise else {
            advertiser?.stop()
            advertiser = nil
            return
        }
        advertiser = MCAdvertiserAssistant(serviceType: "snapshare", discoveryInfo: nil, session: session)
        advertiser?.start()
    }
    
}


// MARK: - MCSessionDelegate
extension MultiPeerSession: MCSessionDelegate {
    
    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
        DispatchQueue.main.async {
            self.delegate?.changedState(state: state, of: peerID)
        }
    }
    
    func session(_ session: MCSession, didReceiveCertificate certificate: [Any]?, fromPeer peerID: MCPeerID, certificateHandler: @escaping (Bool) -> Void) {
        
        return certificateHandler(true)
    }
    
    func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
        DispatchQueue.main.async {
            self.delegate?.receivedData(data: data, from: peerID)
        }
    }
    
    func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
        
    }
    
    func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) {
        
    }
    
    func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL?, withError error: Error?) {
        
    }
    
}
