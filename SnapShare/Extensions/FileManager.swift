//
//  FileManager.swift
//  SnapShare
//
//  Created by Muhammad Shahzad on 12/01/2022.
//  Copyright © 2022 Muhammad Shahzad. All rights reserved.
//

import Foundation

extension FileManager {
    public static func concurrentPath(forResourceWithName name: String, usingConfiguration configuration: ConcurrentConfiguration = ConcurrentConfiguration.default) -> URL? {
        let fileManager = DownloadsFileManager(withBaseDownloadsDirectoryName: configuration.baseDownloadsDirectoryName)
        guard let downloads = try? fileManager.downloadsDirectory() else { return nil }
        let path = downloads.appendingPathComponent(name)
        guard FileManager.default.fileExists(atPath: path.path) else { return nil }
        return path
    }
}
