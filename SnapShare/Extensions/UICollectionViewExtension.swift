//
//  UICollectionViewCell.swift
//  SnapShare
//
//  Created by Muhammad Shazhad on 12/01/2022.
//

import Foundation
import UIKit

extension UICollectionViewCell {
    
    class var reuseIdentifier : String {
        return "\(self)"
    }
}

//MARK:- UICollectionViewCell
extension UICollectionView {
    
    public func register<T: UICollectionViewCell>(cellClass: T.Type) {
        register(UINib(nibName: cellClass.reuseIdentifier, bundle: nil), forCellWithReuseIdentifier: cellClass.reuseIdentifier)
    }
    
    public func dequeue<T: UICollectionViewCell>(cellClass: T.Type, forIndexPath indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(
            withReuseIdentifier: cellClass.reuseIdentifier, for: indexPath) as? T else {
                fatalError(
                    "Error: cell with id: \(cellClass.reuseIdentifier) for indexPath: \(indexPath) is not \(T.self)")
            }
        return cell
    }
    
    func reloadData(_ completion: @escaping () -> Void) {
        UIView.animate(withDuration: 0, animations: {
            self.reloadData()
        }, completion: { _ in
            completion()
        })
    }
}
